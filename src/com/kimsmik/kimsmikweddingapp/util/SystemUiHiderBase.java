// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   SystemUiHiderBase.java

package com.kimsmik.kimsmikweddingapp.util;

import android.app.Activity;
import android.view.View;
import android.view.Window;

// Referenced classes of package com.kimsmik.kimsmikweddingapp.util:
//            SystemUiHider

public class SystemUiHiderBase extends SystemUiHider
{

    protected SystemUiHiderBase(Activity activity, View view, int i)
    {
        super(activity, view, i);
        mVisible = true;
    }

    public void hide()
    {
        if((2 & mFlags) != 0)
            mActivity.getWindow().setFlags(1024, 1024);
        mOnVisibilityChangeListener.onVisibilityChange(false);
        mVisible = false;
    }

    public boolean isVisible()
    {
        return mVisible;
    }

    public void setup()
    {
        if((1 & mFlags) == 0)
            mActivity.getWindow().setFlags(768, 768);
    }

    public void show()
    {
        if((2 & mFlags) != 0)
            mActivity.getWindow().setFlags(0, 1024);
        mOnVisibilityChangeListener.onVisibilityChange(true);
        mVisible = true;
    }

    private boolean mVisible;
}
