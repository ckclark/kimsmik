// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   SystemUiHider.java

package com.kimsmik.kimsmikweddingapp.util;

import android.app.Activity;
import android.view.View;

// Referenced classes of package com.kimsmik.kimsmikweddingapp.util:
//            SystemUiHiderHoneycomb, SystemUiHiderBase

public abstract class SystemUiHider
{
    public static interface OnVisibilityChangeListener
    {

        public abstract void onVisibilityChange(boolean flag);
    }


    protected SystemUiHider(Activity activity, View view, int i)
    {
        mOnVisibilityChangeListener = sDummyListener;
        mActivity = activity;
        mAnchorView = view;
        mFlags = i;
    }

    public static SystemUiHider getInstance(Activity activity, View view, int i)
    {
        Object obj;
        if(android.os.Build.VERSION.SDK_INT >= 11)
            obj = new SystemUiHiderHoneycomb(activity, view, i);
        else
            obj = new SystemUiHiderBase(activity, view, i);
        return ((SystemUiHider) (obj));
    }

    public abstract void hide();

    public abstract boolean isVisible();

    public void setOnVisibilityChangeListener(OnVisibilityChangeListener onvisibilitychangelistener)
    {
        if(onvisibilitychangelistener == null)
            onvisibilitychangelistener = sDummyListener;
        mOnVisibilityChangeListener = onvisibilitychangelistener;
    }

    public abstract void setup();

    public abstract void show();

    public void toggle()
    {
        if(isVisible())
            hide();
        else
            show();
    }

    public static final int FLAG_FULLSCREEN = 2;
    public static final int FLAG_HIDE_NAVIGATION = 6;
    public static final int FLAG_LAYOUT_IN_SCREEN_OLDER_DEVICES = 1;
    private static OnVisibilityChangeListener sDummyListener = new OnVisibilityChangeListener() {

        public void onVisibilityChange(boolean flag)
        {
        }

    };
    protected Activity mActivity;
    protected View mAnchorView;
    protected int mFlags;
    protected OnVisibilityChangeListener mOnVisibilityChangeListener;

}
