// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   SystemUiHiderHoneycomb.java

package com.kimsmik.kimsmikweddingapp.util;

import android.app.ActionBar;
import android.app.Activity;
import android.view.View;
import android.view.Window;

// Referenced classes of package com.kimsmik.kimsmikweddingapp.util:
//            SystemUiHiderBase

public class SystemUiHiderHoneycomb extends SystemUiHiderBase
{

    protected SystemUiHiderHoneycomb(Activity activity, View view, int i)
    {
        super(activity, view, i);
        mVisible = true;
        mSystemUiVisibilityChangeListener = new android.view.View.OnSystemUiVisibilityChangeListener() {

            public void onSystemUiVisibilityChange(int j)
            {
                if((j & mTestFlags) != 0)
                {
                    if(android.os.Build.VERSION.SDK_INT < 16)
                    {
                        mActivity.getActionBar().hide();
                        mActivity.getWindow().setFlags(1024, 1024);
                    }
                    mOnVisibilityChangeListener.onVisibilityChange(false);
                    mVisible = false;
                } else
                {
                    mAnchorView.setSystemUiVisibility(mShowFlags);
                    if(android.os.Build.VERSION.SDK_INT < 16)
                    {
                        mActivity.getActionBar().show();
                        mActivity.getWindow().setFlags(0, 1024);
                    }
                    mOnVisibilityChangeListener.onVisibilityChange(true);
                    mVisible = true;
                }
            }

            final SystemUiHiderHoneycomb this$0;

            
            {
                this$0 = SystemUiHiderHoneycomb.this;
                super();
            }
        };
        mShowFlags = 0;
        mHideFlags = 1;
        mTestFlags = 1;
        if((2 & mFlags) != 0)
        {
            mShowFlags = 0x400 | mShowFlags;
            mHideFlags = 0x404 | mHideFlags;
        }
        if((6 & mFlags) != 0)
        {
            mShowFlags = 0x200 | mShowFlags;
            mHideFlags = 0x202 | mHideFlags;
            mTestFlags = 2 | mTestFlags;
        }
    }

    public void hide()
    {
        mAnchorView.setSystemUiVisibility(mHideFlags);
    }

    public boolean isVisible()
    {
        return mVisible;
    }

    public void setup()
    {
        mAnchorView.setOnSystemUiVisibilityChangeListener(mSystemUiVisibilityChangeListener);
    }

    public void show()
    {
        mAnchorView.setSystemUiVisibility(mShowFlags);
    }

    private int mHideFlags;
    private int mShowFlags;
    private android.view.View.OnSystemUiVisibilityChangeListener mSystemUiVisibilityChangeListener;
    private int mTestFlags;
    private boolean mVisible;



/*
    static boolean access$102(SystemUiHiderHoneycomb systemuihiderhoneycomb, boolean flag)
    {
        systemuihiderhoneycomb.mVisible = flag;
        return flag;
    }

*/

}
