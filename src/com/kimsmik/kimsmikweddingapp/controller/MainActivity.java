// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   MainActivity.java

package com.kimsmik.kimsmikweddingapp.controller;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.*;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.kimsmik.kimsmikweddingapp.IMenuFragment;
import com.kimsmik.kimsmikweddingapp.service.GCMIntentService;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.kimsmik.kimsmikweddingapp.controller:
//            HomeFragment, QuestStartFragment, MapFragment, MenuFragment, 
//            GalleryFragment

public class MainActivity extends ActionBarActivity
    implements android.support.v7.app.ActionBar.TabListener
{
    public class SectionsPagerAdapter extends FragmentPagerAdapter
    {

        public int getCount()
        {
            return menuFragments.size();
        }

        public Fragment getItem(int i)
        {
            return (Fragment)(Fragment)menuFragments.get(i);
        }

        public CharSequence getPageTitle(int i)
        {
            String s;
            if(menuFragments.get(i) != null)
                s = ((IMenuFragment)menuFragments.get(i)).GetTitle();
            else
                s = null;
            return s;
        }

        List menuFragments;
        final MainActivity this$0;

        public SectionsPagerAdapter(FragmentManager fragmentmanager)
        {
            this$0 = MainActivity.this;
            super(fragmentmanager);
            menuFragments = new ArrayList();
            menuFragments.add(new HomeFragment());
            menuFragments.add(new QuestStartFragment());
            menuFragments.add(new MapFragment());
            menuFragments.add(new MenuFragment());
            menuFragments.add(new GalleryFragment());
        }
    }


    public MainActivity()
    {
    }

    private boolean checkPlayServices()
    {
        int i = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        boolean flag;
        if(i != 0)
        {
            if(GooglePlayServicesUtil.isUserRecoverableError(i))
                GooglePlayServicesUtil.getErrorDialog(i, this, 9000).show();
            else
                finish();
            flag = false;
        } else
        {
            flag = true;
        }
        return flag;
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f040019);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(2);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager)findViewById(0x7f0b0067);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new android.support.v4.view.ViewPager.SimpleOnPageChangeListener() {

            public void onPageSelected(int j)
            {
                actionBar.setSelectedNavigationItem(j);
            }

            final MainActivity this$0;
            final ActionBar val$actionBar;

            
            {
                this$0 = MainActivity.this;
                actionBar = actionbar;
                super();
            }
        });
        for(int i = 0; i < mSectionsPagerAdapter.getCount(); i++)
            actionBar.addTab(actionBar.newTab().setText(mSectionsPagerAdapter.getPageTitle(i)).setTabListener(this));

        if(checkPlayServices())
            startService(new Intent(this, com/kimsmik/kimsmikweddingapp/service/GCMIntentService));
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuitem)
    {
        boolean flag;
        if(menuitem.getItemId() == 0x7f0b0097)
            flag = true;
        else
            flag = super.onOptionsItemSelected(menuitem);
        return flag;
    }

    public void onTabReselected(android.support.v7.app.ActionBar.Tab tab, FragmentTransaction fragmenttransaction)
    {
    }

    public void onTabSelected(android.support.v7.app.ActionBar.Tab tab, FragmentTransaction fragmenttransaction)
    {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    public void onTabUnselected(android.support.v7.app.ActionBar.Tab tab, FragmentTransaction fragmenttransaction)
    {
    }

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
}
