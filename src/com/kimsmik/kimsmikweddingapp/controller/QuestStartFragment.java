// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   QuestStartFragment.java

package com.kimsmik.kimsmikweddingapp.controller;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.*;
import android.view.*;
import android.widget.ImageView;
import com.kimsmik.kimsmikweddingapp.IMenuFragment;

// Referenced classes of package com.kimsmik.kimsmikweddingapp.controller:
//            QuestionFragment

public class QuestStartFragment extends Fragment
    implements IMenuFragment
{
    public static interface QuizStartListener
    {

        public abstract void OnStart();
    }


    public QuestStartFragment()
    {
        startListener = null;
        uiContainer = null;
        fragContainer = null;
    }

    public String GetTitle()
    {
        return "有獎問答";
    }

    public void SetOnQuizStartListener(QuizStartListener quizstartlistener)
    {
        startListener = quizstartlistener;
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        View view = layoutinflater.inflate(0x7f040022, viewgroup, false);
        clearView = (ImageView)view.findViewById(0x7f0b0074);
        if(getActivity().getPreferences(0).getBoolean(PREF_STAGE_CLEAR, false))
            clearView.setVisibility(0);
        else
            clearView.setVisibility(4);
        uiContainer = (ViewGroup)view.findViewById(0x7f0b0070);
        fragContainer = (ViewGroup)view.findViewById(0x7f0b006f);
        ((ImageView)view.findViewById(0x7f0b0072)).setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View view1)
            {
                uiContainer.setVisibility(4);
                final QuestionFragment quesFrag = new QuestionFragment();
                quesFrag.SetOnQuizFinishListener(new QuestionFragment.QuizFinishListener() {

                    public void OnFinish(boolean flag)
                    {
                        uiContainer.setVisibility(0);
                        getActivity().getSupportFragmentManager().beginTransaction().detach(quesFrag).commit();
                        if(flag)
                        {
                            getActivity().getPreferences(0).edit().putBoolean(QuestStartFragment.PREF_STAGE_CLEAR, true).commit();
                            clearView.setVisibility(0);
                        }
                    }

                    final _cls1 this$1;
                    final QuestionFragment val$quesFrag;

                    
                    {
                        this$1 = _cls1.this;
                        quesFrag = questionfragment;
                        super();
                    }
                });
                getActivity().getSupportFragmentManager().beginTransaction().replace(0x7f0b006f, quesFrag).commit();
            }

            final QuestStartFragment this$0;

            
            {
                this$0 = QuestStartFragment.this;
                super();
            }
        });
        return view;
    }

    private static String PREF_STAGE_CLEAR = "quiz_stage_clear";
    private ImageView clearView;
    private ViewGroup fragContainer;
    private QuizStartListener startListener;
    private ViewGroup uiContainer;




}
