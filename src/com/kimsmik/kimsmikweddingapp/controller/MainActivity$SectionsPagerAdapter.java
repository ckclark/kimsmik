// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   MainActivity.java

package com.kimsmik.kimsmikweddingapp.controller;

import android.support.v4.app.*;
import com.kimsmik.kimsmikweddingapp.IMenuFragment;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.kimsmik.kimsmikweddingapp.controller:
//            MainActivity, HomeFragment, QuestStartFragment, MapFragment, 
//            MenuFragment, GalleryFragment

public class menuFragments extends FragmentPagerAdapter
{

    public int getCount()
    {
        return menuFragments.size();
    }

    public Fragment getItem(int i)
    {
        return (Fragment)(Fragment)menuFragments.get(i);
    }

    public CharSequence getPageTitle(int i)
    {
        String s;
        if(menuFragments.get(i) != null)
            s = ((IMenuFragment)menuFragments.get(i)).GetTitle();
        else
            s = null;
        return s;
    }

    List menuFragments;
    final MainActivity this$0;

    public (FragmentManager fragmentmanager)
    {
        this$0 = MainActivity.this;
        super(fragmentmanager);
        menuFragments = new ArrayList();
        menuFragments.add(new HomeFragment());
        menuFragments.add(new QuestStartFragment());
        menuFragments.add(new MapFragment());
        menuFragments.add(new MenuFragment());
        menuFragments.add(new GalleryFragment());
    }
}
