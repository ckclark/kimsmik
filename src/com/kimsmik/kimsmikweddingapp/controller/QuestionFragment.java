// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   QuestionFragment.java

package com.kimsmik.kimsmikweddingapp.controller;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParserException;

public class QuestionFragment extends Fragment
{
    public static interface QuizFinishListener
    {

        public abstract void OnFinish(boolean flag);
    }

    public class Question
    {

        String answer;
        List option;
        String quest;
        final QuestionFragment this$0;

        public Question()
        {
            this$0 = QuestionFragment.this;
            super();
            quest = "";
            option = new ArrayList();
            answer = "";
        }
    }


    public QuestionFragment()
    {
        questions = new ArrayList();
        nowQuestIndex = 0;
        correctNum = 0;
        finishListener = null;
    }

    private void CheckAns(TextView textview)
    {
        Question question = (Question)questions.get(nowQuestIndex);
        if(textview.getText().equals(question.answer))
            correctNum = 1 + correctNum;
        nowQuestIndex = 1 + nowQuestIndex;
        SetNextQuestion();
    }

    private String GetResultString()
    {
        String s = (new StringBuilder()).append("答對題數 : ").append(correctNum).append("/").append(questions.size()).append("\n\n").toString();
        float f = (float)correctNum / (float)questions.size();
        String s1;
        if(f >= 1F)
            s1 = (new StringBuilder()).append(s).append("恭喜你答對所有題目\n你真是太了解新人了\n婚禮當天請至現場\n出示App內的通關證明\n換取神祕小禮").toString();
        else
        if((double)f >= 0.80000000000000004D)
            s1 = (new StringBuilder()).append(s).append("真是太可惜了\n只差一點點就全部正解了\n請再接再厲").toString();
        else
        if((double)f >= 0.5D)
            s1 = (new StringBuilder()).append(s).append("看來你對新人還是一知半解的喔\n但沒關係\n請再多試幾次\n也可更了解新人喔").toString();
        else
            s1 = (new StringBuilder()).append(s).append("哎呀\n你對新人不是很了解的樣子呢\n這樣子不太行唷\n請多試幾次\n也許你會慢慢了解新人喔").toString();
        return s1;
    }

    private void SetNextQuestion()
    {
        if(nowQuestIndex >= questions.size())
        {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
            builder.setPositiveButton("確定", new android.content.DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialoginterface, int j)
                {
                    if(finishListener != null)
                        if(correctNum == questions.size())
                            finishListener.OnFinish(true);
                        else
                            finishListener.OnFinish(false);
                }

                final QuestionFragment this$0;

            
            {
                this$0 = QuestionFragment.this;
                super();
            }
            });
            builder.setMessage(GetResultString());
            builder.setTitle("答題結果");
            builder.show();
        } else
        {
            Question question = (Question)questions.get(nowQuestIndex);
            questText.setText(question.quest);
            int i = 0;
            while(i < optionGroup.getChildCount()) 
            {
                TextView textview = (TextView)optionGroup.getChildAt(i);
                if(i >= question.option.size())
                {
                    textview.setVisibility(4);
                } else
                {
                    textview.setText((CharSequence)question.option.get(i));
                    textview.setVisibility(0);
                }
                i++;
            }
            progressText.setText((new StringBuilder()).append(1 + nowQuestIndex).append("/").append(questions.size()).toString());
        }
    }

    public void SetOnQuizFinishListener(QuizFinishListener quizfinishlistener)
    {
        finishListener = quizfinishlistener;
    }

    public void onCreate(Bundle bundle)
    {
        XmlResourceParser xmlresourceparser;
        Question question;
        super.onCreate(bundle);
        xmlresourceparser = getResources().getXml(0x7f060000);
        question = null;
_L7:
        if(xmlresourceparser.getEventType() == 1) goto _L2; else goto _L1
_L1:
        if(xmlresourceparser.getEventType() != 2) goto _L4; else goto _L3
_L3:
        if(!xmlresourceparser.getName().equals("Quest")) goto _L6; else goto _L5
_L5:
        Question question1 = new Question();
        question1.quest = xmlresourceparser.getAttributeValue(null, "text");
_L9:
        xmlresourceparser.next();
        question = question1;
          goto _L7
_L6:
        if(xmlresourceparser.getName().equals("Option"))
        {
            String s = xmlresourceparser.getAttributeValue(null, "text");
            question.option.add(s);
            if(xmlresourceparser.getAttributeBooleanValue(null, "answer", false))
                question.answer = s;
            break MISSING_BLOCK_LABEL_220;
        }
          goto _L8
_L4:
        if(xmlresourceparser.getEventType() == 3 && xmlresourceparser.getName().equals("Quest"))
            questions.add(question);
_L8:
        question1 = question;
          goto _L9
        XmlPullParserException xmlpullparserexception;
        xmlpullparserexception;
        question;
_L13:
        xmlpullparserexception.printStackTrace();
_L2:
        return;
        IOException ioexception;
        ioexception;
        question;
_L11:
        ioexception.printStackTrace();
        if(true) goto _L2; else goto _L10
_L10:
        ioexception;
          goto _L11
        xmlpullparserexception;
        if(true) goto _L13; else goto _L12
_L12:
        question1 = question;
          goto _L9
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        nowQuestIndex = 0;
        correctNum = 0;
        View view = layoutinflater.inflate(0x7f040023, viewgroup, false);
        optionGroup = (LinearLayout)(LinearLayout)view.findViewById(0x7f0b0078);
        questText = (TextView)(TextView)view.findViewById(0x7f0b0077);
        progressText = (TextView)(TextView)view.findViewById(0x7f0b0075);
        for(int i = 0; i < optionGroup.getChildCount(); i++)
            ((TextView)optionGroup.getChildAt(i)).setOnClickListener(new android.view.View.OnClickListener() {

                public void onClick(View view1)
                {
                    CheckAns((TextView)view1);
                }

                final QuestionFragment this$0;

            
            {
                this$0 = QuestionFragment.this;
                super();
            }
            });

        SetNextQuestion();
        return view;
    }

    public void onPause()
    {
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
    }

    private int correctNum;
    private QuizFinishListener finishListener;
    private int nowQuestIndex;
    private LinearLayout optionGroup;
    private TextView progressText;
    private TextView questText;
    private List questions;




}
