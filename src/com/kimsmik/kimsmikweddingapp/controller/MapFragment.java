// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   MapFragment.java

package com.kimsmik.kimsmikweddingapp.controller;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import com.kimsmik.kimsmikweddingapp.IMenuFragment;

public class MapFragment extends Fragment
    implements IMenuFragment
{

    public MapFragment()
    {
    }

    public String GetTitle()
    {
        return "會場資訊";
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        return layoutinflater.inflate(0x7f040020, viewgroup, false);
    }

    public void onDestroy()
    {
        super.onDestroy();
    }
}
