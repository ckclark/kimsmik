// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   ScrollingActivity.java

package com.kimsmik.kimsmikweddingapp.controller;

import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.*;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.*;
import android.widget.Button;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.kimsmik.kimsmikweddingapp.service.GCMIntentService;
import java.util.Locale;

// Referenced classes of package com.kimsmik.kimsmikweddingapp.controller:
//            ScrollingTextActivity

public class ScrollingActivity extends ActionBarActivity
{
    public static class PlaceholderFragment extends Fragment
    {

        public static PlaceholderFragment newInstance(int i)
        {
            PlaceholderFragment placeholderfragment = new PlaceholderFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("section_number", i);
            placeholderfragment.setArguments(bundle);
            return placeholderfragment;
        }

        public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
        {
            View view = layoutinflater.inflate(0x7f04001e, viewgroup, false);
            view.setSystemUiVisibility(4);
            int i = getArguments().getInt("section_number");
            ((TextView)view.findViewById(0x7f0b006d)).setText((new StringBuilder()).append("Section : ").append(i).toString());
            if(i == 1)
            {
                Button button = new Button(getActivity());
                button.setText("Scrolling Text");
                button.setLayoutParams(new android.view.ViewGroup.LayoutParams(-2, -2));
                button.setOnClickListener(new android.view.View.OnClickListener() {

                    public void onClick(View view1)
                    {
                        Intent intent = new Intent();
                        intent.setClass(getActivity(), com/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity);
                        startActivity(intent);
                    }

                    final PlaceholderFragment this$0;

                
                {
                    this$0 = PlaceholderFragment.this;
                    super();
                }
                });
                ((ViewGroup)view).addView(button);
            }
            return view;
        }

        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment()
        {
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter
    {

        public int getCount()
        {
            return 3;
        }

        public Fragment getItem(int i)
        {
            return PlaceholderFragment.newInstance(i + 1);
        }

        public CharSequence getPageTitle(int i)
        {
            Locale locale = Locale.getDefault();
            i;
            JVM INSTR tableswitch 0 2: default 32
        //                       0 36
        //                       1 53
        //                       2 70;
               goto _L1 _L2 _L3 _L4
_L1:
            Object obj = null;
_L6:
            return ((CharSequence) (obj));
_L2:
            obj = getString(0x7f0d004f).toUpperCase(locale);
            continue; /* Loop/switch isn't completed */
_L3:
            obj = getString(0x7f0d0050).toUpperCase(locale);
            continue; /* Loop/switch isn't completed */
_L4:
            obj = getString(0x7f0d0051).toUpperCase(locale);
            if(true) goto _L6; else goto _L5
_L5:
        }

        final ScrollingActivity this$0;

        public SectionsPagerAdapter(FragmentManager fragmentmanager)
        {
            this$0 = ScrollingActivity.this;
            super(fragmentmanager);
        }
    }


    public ScrollingActivity()
    {
    }

    private boolean checkPlayServices()
    {
        int i = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        boolean flag;
        if(i != 0)
        {
            if(GooglePlayServicesUtil.isUserRecoverableError(i))
                GooglePlayServicesUtil.getErrorDialog(i, this, 9000).show();
            else
                finish();
            flag = false;
        } else
        {
            flag = true;
        }
        return flag;
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f04001a);
        float af[] = new float[2];
        af[0] = 0F;
        af[1] = 2000F;
        ValueAnimator valueanimator = ValueAnimator.ofFloat(af);
        valueanimator.setDuration(1000L);
        valueanimator.addUpdateListener(new android.animation.ValueAnimator.AnimatorUpdateListener() {

            public void onAnimationUpdate(ValueAnimator valueanimator1)
            {
            }

            final ScrollingActivity this$0;

            
            {
                this$0 = ScrollingActivity.this;
                super();
            }
        });
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager)findViewById(0x7f0b0067);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        if(checkPlayServices())
            startService(new Intent(this, com/kimsmik/kimsmikweddingapp/service/GCMIntentService));
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(0x7f0f0001, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuitem)
    {
        boolean flag;
        if(menuitem.getItemId() == 0x7f0b0097)
            flag = true;
        else
            flag = super.onOptionsItemSelected(menuitem);
        return flag;
    }

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
}
