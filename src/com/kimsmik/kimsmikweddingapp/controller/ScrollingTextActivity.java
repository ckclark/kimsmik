// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   ScrollingTextActivity.java

package com.kimsmik.kimsmikweddingapp.controller;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import com.kimsmik.kimsmikweddingapp.object.MarqueeTextView;
import com.kimsmik.kimsmikweddingapp.util.SystemUiHider;

public class ScrollingTextActivity extends Activity
{

    public ScrollingTextActivity()
    {
        isLightOn = false;
        mHideHandler = new Handler();
        mHideRunnable = new Runnable() {

            public void run()
            {
                mSystemUiHider.hide();
            }

            final ScrollingTextActivity this$0;

            
            {
                this$0 = ScrollingTextActivity.this;
                super();
            }
        };
    }

    private void delayedHide(int i)
    {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, i);
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        getActionBar().hide();
        String s = getIntent().getStringExtra("message");
        setContentView(0x7f04001b);
        srollingText = (MarqueeTextView)findViewById(0x7f0b0068);
        if(s != null && !s.equals(""))
            srollingText.setText(s);
        srollingText.scrollText(2F);
        mSystemUiHider = SystemUiHider.getInstance(this, findViewById(0x7f0b0066), 6);
        mSystemUiHider.setup();
    }

    protected void onPause()
    {
        super.onPause();
        if(camera != null && isLightOn)
        {
            android.hardware.Camera.Parameters parameters = camera.getParameters();
            parameters.setFlashMode("off");
            camera.setParameters(parameters);
            camera.stopPreview();
            isLightOn = false;
        }
        if(camera != null)
        {
            camera.release();
            camera = null;
        }
    }

    protected void onPostCreate(Bundle bundle)
    {
        super.onPostCreate(bundle);
        delayedHide(100);
    }

    protected void onResume()
    {
        super.onResume();
        if(getPackageManager().hasSystemFeature("android.hardware.camera"))
            camera = Camera.open();
        if(camera != null)
        {
            android.hardware.Camera.Parameters parameters = camera.getParameters();
            parameters.setFlashMode("torch");
            camera.setParameters(parameters);
            camera.startPreview();
            isLightOn = true;
        }
    }

    protected void onStop()
    {
        super.onStop();
    }

    private static final int HIDER_FLAGS = 6;
    private Camera camera;
    private boolean isLightOn;
    Handler mHideHandler;
    Runnable mHideRunnable;
    private SystemUiHider mSystemUiHider;
    private MarqueeTextView srollingText;

}
