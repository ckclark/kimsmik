// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   GalleryFragment.java

package com.kimsmik.kimsmikweddingapp.controller;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.*;
import com.kimsmik.kimsmikweddingapp.IMenuFragment;
import java.util.ArrayList;
import java.util.List;

public class GalleryFragment extends Fragment
    implements IMenuFragment
{

    public GalleryFragment()
    {
        photoView = null;
        photoNum = 0;
        photoIndex = 0;
    }

    private static Bitmap createMiniMap(Resources resources, int i, double d, double d1)
    {
        android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        BitmapFactory.decodeResource(resources, i, options);
        int j = (int)Math.ceil((double)options.outHeight / d1);
        int k = (int)Math.ceil((double)options.outWidth / d);
        if(j > 1 || k > 1)
            if(j > k)
                options.inSampleSize = j;
            else
                options.inSampleSize = k;
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(resources, i, options);
    }

    private static void initMinmap(Resources resources)
    {
        for(int i = 0; i < photoList.length; i++)
        {
            Bitmap bitmap = createMiniMap(resources, photoList[i], 50D, 50D);
            minmapCache.add(bitmap);
        }

    }

    private void showPhoto(int i)
    {
        System.gc();
        if(i >= 0 && i < photoList.length)
            photoView.setImageResource(photoList[i]);
    }

    public String GetTitle()
    {
        return "婚照欣賞";
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        System.gc();
        View view = layoutinflater.inflate(0x7f04001c, viewgroup, false);
        photoView = (ImageView)view.findViewById(0x7f0b0069);
        photoNum = photoList.length;
        showPhoto(photoIndex);
        LinearLayout linearlayout = new LinearLayout(getActivity());
        HorizontalScrollView horizontalscrollview = (HorizontalScrollView)view.findViewById(0x7f0b006a);
        if(!inited)
        {
            initMinmap(getResources());
            inited = true;
        }
        for(int i = 0; i < minmapCache.size(); i++)
        {
            final int index = i;
            Bitmap bitmap = (Bitmap)minmapCache.get(i);
            ImageView imageview = new ImageView(getActivity());
            imageview.setPadding(3, 2, 3, 2);
            imageview.setImageBitmap(bitmap);
            imageview.setAdjustViewBounds(true);
            imageview.setOnClickListener(new android.view.View.OnClickListener() {

                public void onClick(View view1)
                {
                    showPhoto(index);
                }

                final GalleryFragment this$0;
                final int val$index;

            
            {
                this$0 = GalleryFragment.this;
                index = i;
                super();
            }
            });
            linearlayout.addView(imageview, -1, -1);
        }

        horizontalscrollview.addView(linearlayout, -1, -1);
        return view;
    }

    public void onDestroy()
    {
        super.onDestroy();
    }

    private static boolean inited = false;
    private static List minmapCache = new ArrayList();
    private static int photoList[];
    private int photoIndex;
    private int photoNum;
    private ImageView photoView;

    static 
    {
        int ai[] = new int[15];
        ai[0] = 0x7f020084;
        ai[1] = 0x7f020086;
        ai[2] = 0x7f020088;
        ai[3] = 0x7f02008a;
        ai[4] = 0x7f02008c;
        ai[5] = 0x7f02008e;
        ai[6] = 0x7f020090;
        ai[7] = 0x7f020092;
        ai[8] = 0x7f020094;
        ai[9] = 0x7f020096;
        ai[10] = 0x7f020098;
        ai[11] = 0x7f02009a;
        ai[12] = 0x7f02009c;
        ai[13] = 0x7f02009e;
        ai[14] = 0x7f0200a0;
        photoList = ai;
    }

}
