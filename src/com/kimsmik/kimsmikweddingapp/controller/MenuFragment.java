// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   MenuFragment.java

package com.kimsmik.kimsmikweddingapp.controller;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import com.kimsmik.kimsmikweddingapp.IMenuFragment;

public class MenuFragment extends Fragment
    implements IMenuFragment
{

    public MenuFragment()
    {
    }

    public String GetTitle()
    {
        return "婚宴菜單";
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        return layoutinflater.inflate(0x7f040021, viewgroup, false);
    }

    public void onDestroy()
    {
        super.onDestroy();
    }
}
