// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   MarqueeTextView.java

package com.kimsmik.kimsmikweddingapp.object;

import android.content.Context;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.Scroller;
import android.widget.TextView;

public class MarqueeTextView extends TextView
{

    public MarqueeTextView(Context context)
    {
        super(context);
        mContext = context;
        setSingleLine();
        mScroller = new Scroller(context, new LinearInterpolator());
        setScroller(mScroller);
    }

    public MarqueeTextView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        mContext = context;
        setSingleLine();
        mScroller = new Scroller(context, new LinearInterpolator());
        setScroller(mScroller);
    }

    public MarqueeTextView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mContext = context;
        setSingleLine();
        mScroller = new Scroller(context, new LinearInterpolator());
        setScroller(mScroller);
    }

    private int calculateMoveDistance(boolean flag, float f)
    {
        Rect rect = new Rect();
        String s = (String)getText();
        getPaint().getTextBounds(s, 0, s.length(), rect);
        int i = rect.width();
        if(!flag)
            i += getWidth();
        mDistance = i;
        mDuration = (int)(f * (float)mDistance);
        return mDistance;
    }

    public void computeScroll()
    {
        super.computeScroll();
        if(!mScroller.computeScrollOffset()) goto _L2; else goto _L1
_L1:
        scrollTo(mScroller.getCurrX(), mScroller.getCurrY());
_L4:
        return;
_L2:
        if(mScroller.isFinished())
            mScroller.startScroll(-getWidth(), 0, calculateMoveDistance(false, mVelocity), 0, mDuration);
        if(true) goto _L4; else goto _L3
_L3:
    }

    public void scrollText(float f)
    {
        mVelocity = f;
        mScroller.startScroll(0, 0, calculateMoveDistance(true, f), 0, mDuration);
    }

    private Context mContext;
    private int mDistance;
    private int mDuration;
    private Scroller mScroller;
    private float mVelocity;
}
