// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   KimSmikService.java

package com.kimsmik.kimsmikweddingapp.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class KimSmikService extends Service
{
    private class ServiceBinder extends Binder
    {

        public KimSmikService GetService()
        {
            return KimSmikService.this;
        }

        final KimSmikService this$0;

        private ServiceBinder()
        {
            this$0 = KimSmikService.this;
            super();
        }

    }


    public KimSmikService()
    {
        mBinder = new ServiceBinder();
    }

    public IBinder onBind(Intent intent)
    {
        return mBinder;
    }

    private ServiceBinder mBinder;
}
