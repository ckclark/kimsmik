// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   GCMReceiverService.java

package com.kimsmik.kimsmikweddingapp.service;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.gcm.GcmListenerService;
import com.kimsmik.kimsmikweddingapp.controller.ScrollingTextActivity;

public class GCMReceiverService extends GcmListenerService
{

    public GCMReceiverService()
    {
    }

    public void onMessageReceived(String s, Bundle bundle)
    {
        String s1 = bundle.getString("message");
        Log.d(getClass().getName(), (new StringBuilder()).append("From: ").append(s).toString());
        Log.d(getClass().getName(), (new StringBuilder()).append("Message: ").append(s1).toString());
        Intent intent = new Intent();
        intent.putExtra("message", s1);
        intent.setFlags(0x10008000);
        intent.setClass(this, com/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity);
        startActivity(intent);
    }
}
