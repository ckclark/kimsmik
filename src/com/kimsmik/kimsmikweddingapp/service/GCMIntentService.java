// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   GCMIntentService.java

package com.kimsmik.kimsmikweddingapp.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.iid.InstanceID;

public class GCMIntentService extends IntentService
{

    public GCMIntentService()
    {
        super("GCMIntentService");
    }

    protected void onHandleIntent(Intent intent)
    {
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this;
        JVM INSTR monitorenter ;
        GcmPubSub.getInstance(this).subscribe(InstanceID.getInstance(this).getToken("336172888047", "GCM", null), (new StringBuilder()).append("/topics/").append(GCM_TOPIC).toString(), null);
        sharedpreferences.edit().putBoolean("SENT_TO_SERVER", true).apply();
        break MISSING_BLOCK_LABEL_99;
        Exception exception;
        exception;
        sharedpreferences.edit().putBoolean("SENT_TO_SERVER", false).apply();
    }

    private static String GCM_TOPIC = "kimsmik_wedding";

}
