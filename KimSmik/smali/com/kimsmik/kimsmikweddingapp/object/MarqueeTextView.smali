.class public Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;
.super Landroid/widget/TextView;
.source "MarqueeTextView.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDistance:I

.field private mDuration:I

.field private mScroller:Landroid/widget/Scroller;

.field private mVelocity:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 22
    iput-object p1, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mContext:Landroid/content/Context;

    .line 23
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->setSingleLine()V

    .line 24
    new-instance v0, Landroid/widget/Scroller;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-direct {v0, p1, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mScroller:Landroid/widget/Scroller;

    .line 25
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0, v0}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->setScroller(Landroid/widget/Scroller;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    iput-object p1, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mContext:Landroid/content/Context;

    .line 31
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->setSingleLine()V

    .line 32
    new-instance v0, Landroid/widget/Scroller;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-direct {v0, p1, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mScroller:Landroid/widget/Scroller;

    .line 33
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0, v0}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->setScroller(Landroid/widget/Scroller;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    iput-object p1, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mContext:Landroid/content/Context;

    .line 39
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->setSingleLine()V

    .line 40
    new-instance v0, Landroid/widget/Scroller;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-direct {v0, p1, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mScroller:Landroid/widget/Scroller;

    .line 41
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0, v0}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->setScroller(Landroid/widget/Scroller;)V

    .line 42
    return-void
.end method

.method private calculateMoveDistance(ZF)I
    .locals 6
    .param p1, "isFirstRun"    # Z
    .param p2, "velocity"    # F

    .prologue
    .line 57
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 58
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 59
    .local v2, "textString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v2, v4, v5, v1}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 60
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 61
    .local v0, "moveDistance":I
    if-eqz p1, :cond_0

    .end local v0    # "moveDistance":I
    :goto_0
    iput v0, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mDistance:I

    .line 62
    iget v3, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mDistance:I

    int-to-float v3, v3

    mul-float/2addr v3, p2

    float-to-int v3, v3

    iput v3, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mDuration:I

    .line 63
    iget v3, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mDistance:I

    return v3

    .line 61
    .restart local v0    # "moveDistance":I
    :cond_0
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->getWidth()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_0
.end method


# virtual methods
.method public computeScroll()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-super {p0}, Landroid/widget/TextView;->computeScroll()V

    .line 47
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->scrollTo(II)V

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->getWidth()I

    move-result v1

    neg-int v1, v1

    iget v3, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mVelocity:F

    invoke-direct {p0, v2, v3}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->calculateMoveDistance(ZF)I

    move-result v3

    iget v5, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mDuration:I

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    goto :goto_0
.end method

.method public scrollText(F)V
    .locals 6
    .param p1, "velocity"    # F

    .prologue
    const/4 v1, 0x0

    .line 67
    iput p1, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mVelocity:F

    .line 68
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mScroller:Landroid/widget/Scroller;

    const/4 v2, 0x1

    invoke-direct {p0, v2, p1}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->calculateMoveDistance(ZF)I

    move-result v3

    iget v5, p0, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->mDuration:I

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 69
    return-void
.end method
