.class Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb$1;
.super Ljava/lang/Object;
.source "SystemUiHiderHoneycomb.java"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;


# direct methods
.method constructor <init>(Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSystemUiVisibilityChange(I)V
    .locals 6
    .param p1, "vis"    # I

    .prologue
    const/16 v5, 0x10

    const/4 v4, 0x1

    const/16 v3, 0x400

    const/4 v2, 0x0

    .line 108
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;

    # getter for: Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->mTestFlags:I
    invoke-static {v0}, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->access$000(Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;)I

    move-result v0

    and-int/2addr v0, p1

    if-eqz v0, :cond_1

    .line 109
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v5, :cond_0

    .line 112
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;

    iget-object v0, v0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 113
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;

    iget-object v0, v0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;

    iget-object v0, v0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->mOnVisibilityChangeListener:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHider$OnVisibilityChangeListener;

    invoke-interface {v0, v2}, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHider$OnVisibilityChangeListener;->onVisibilityChange(Z)V

    .line 121
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;

    # setter for: Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->mVisible:Z
    invoke-static {v0, v2}, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->access$102(Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;Z)Z

    .line 139
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;

    iget-object v0, v0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->mAnchorView:Landroid/view/View;

    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;

    # getter for: Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->mShowFlags:I
    invoke-static {v1}, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->access$200(Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 125
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v5, :cond_2

    .line 128
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;

    iget-object v0, v0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 129
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;

    iget-object v0, v0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/Window;->setFlags(II)V

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;

    iget-object v0, v0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->mOnVisibilityChangeListener:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHider$OnVisibilityChangeListener;

    invoke-interface {v0, v4}, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHider$OnVisibilityChangeListener;->onVisibilityChange(Z)V

    .line 137
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;

    # setter for: Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->mVisible:Z
    invoke-static {v0, v4}, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;->access$102(Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHiderHoneycomb;Z)Z

    goto :goto_0
.end method
