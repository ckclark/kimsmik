.class public Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;
.super Landroid/support/v4/app/Fragment;
.source "QuestStartFragment.java"

# interfaces
.implements Lcom/kimsmik/kimsmikweddingapp/IMenuFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$QuizStartListener;
    }
.end annotation


# static fields
.field private static PREF_STAGE_CLEAR:Ljava/lang/String;


# instance fields
.field private clearView:Landroid/widget/ImageView;

.field private fragContainer:Landroid/view/ViewGroup;

.field private startListener:Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$QuizStartListener;

.field private uiContainer:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "quiz_stage_clear"

    sput-object v0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->PREF_STAGE_CLEAR:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 18
    iput-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->startListener:Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$QuizStartListener;

    .line 19
    iput-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->uiContainer:Landroid/view/ViewGroup;

    .line 20
    iput-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->fragContainer:Landroid/view/ViewGroup;

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->uiContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->PREF_STAGE_CLEAR:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->clearView:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public GetTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const-string v0, "\u6709\u734e\u554f\u7b54"

    return-object v0
.end method

.method public SetOnQuizStartListener(Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$QuizStartListener;)V
    .locals 0
    .param p1, "l"    # Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$QuizStartListener;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->startListener:Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$QuizStartListener;

    .line 74
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 25
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 31
    const v3, 0x7f040022

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 33
    .local v2, "root":Landroid/view/View;
    const v3, 0x7f0b0074

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->clearView:Landroid/widget/ImageView;

    .line 34
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 35
    .local v1, "pref":Landroid/content/SharedPreferences;
    sget-object v3, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->PREF_STAGE_CLEAR:Ljava/lang/String;

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 36
    iget-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->clearView:Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 41
    :goto_0
    const v3, 0x7f0b0070

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->uiContainer:Landroid/view/ViewGroup;

    .line 42
    const v3, 0x7f0b006f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->fragContainer:Landroid/view/ViewGroup;

    .line 44
    const v3, 0x7f0b0072

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 45
    .local v0, "beginBtn":Landroid/widget/ImageView;
    new-instance v3, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1;

    invoke-direct {v3, p0}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1;-><init>(Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    return-object v2

    .line 38
    .end local v0    # "beginBtn":Landroid/widget/ImageView;
    :cond_0
    iget-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->clearView:Landroid/widget/ImageView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
