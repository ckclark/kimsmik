.class public Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;
.super Landroid/support/v4/app/Fragment;
.source "GalleryFragment.java"

# interfaces
.implements Lcom/kimsmik/kimsmikweddingapp/IMenuFragment;


# static fields
.field private static inited:Z

.field private static minmapCache:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private static photoList:[I


# instance fields
.field private photoIndex:I

.field private photoNum:I

.field private photoView:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput-boolean v0, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->inited:Z

    .line 28
    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->photoList:[I

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->minmapCache:Ljava/util/List;

    return-void

    .line 28
    :array_0
    .array-data 4
        0x7f020084
        0x7f020086
        0x7f020088
        0x7f02008a
        0x7f02008c
        0x7f02008e
        0x7f020090
        0x7f020092
        0x7f020094
        0x7f020096
        0x7f020098
        0x7f02009a
        0x7f02009c
        0x7f02009e
        0x7f0200a0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->photoView:Landroid/widget/ImageView;

    .line 35
    iput v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->photoNum:I

    .line 36
    iput v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->photoIndex:I

    return-void
.end method

.method static synthetic access$000(Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->showPhoto(I)V

    return-void
.end method

.method private static createMiniMap(Landroid/content/res/Resources;IDD)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "resId"    # I
    .param p2, "desW"    # D
    .param p4, "desH"    # D

    .prologue
    const/4 v6, 0x1

    .line 85
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 86
    .local v1, "option":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v6, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 87
    iput-boolean v6, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 88
    invoke-static {p0, p1, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 89
    .local v0, "bmp":Landroid/graphics/Bitmap;
    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-double v4, v4

    div-double/2addr v4, p4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 90
    .local v3, "yRatio":I
    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-double v4, v4

    div-double/2addr v4, p2

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v2, v4

    .line 91
    .local v2, "xRatio":I
    if-gt v3, v6, :cond_0

    if-le v2, v6, :cond_1

    .line 92
    :cond_0
    if-le v3, v2, :cond_2

    .line 93
    iput v3, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 98
    :cond_1
    :goto_0
    const/4 v4, 0x0

    iput-boolean v4, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 99
    invoke-static {p0, p1, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 100
    return-object v0

    .line 95
    :cond_2
    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    goto :goto_0
.end method

.method private static initMinmap(Landroid/content/res/Resources;)V
    .locals 8
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    const-wide/high16 v2, 0x4049000000000000L    # 50.0

    .line 78
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    sget-object v0, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->photoList:[I

    array-length v0, v0

    if-ge v7, v0, :cond_0

    .line 79
    sget-object v0, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->photoList:[I

    aget v1, v0, v7

    move-object v0, p0

    move-wide v4, v2

    invoke-static/range {v0 .. v5}, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->createMiniMap(Landroid/content/res/Resources;IDD)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 80
    .local v6, "bmp":Landroid/graphics/Bitmap;
    sget-object v0, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->minmapCache:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 82
    .end local v6    # "bmp":Landroid/graphics/Bitmap;
    :cond_0
    return-void
.end method

.method private showPhoto(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 108
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 109
    if-ltz p1, :cond_0

    sget-object v0, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->photoList:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->photoView:Landroid/widget/ImageView;

    sget-object v1, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->photoList:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public GetTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    const-string v0, "\u5a5a\u7167\u6b23\u8cde"

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 40
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 46
    const v7, 0x7f04001c

    const/4 v8, 0x0

    invoke-virtual {p1, v7, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 47
    .local v6, "root":Landroid/view/View;
    const v7, 0x7f0b0069

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->photoView:Landroid/widget/ImageView;

    .line 48
    sget-object v7, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->photoList:[I

    array-length v7, v7

    iput v7, p0, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->photoNum:I

    .line 49
    iget v7, p0, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->photoIndex:I

    invoke-direct {p0, v7}, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->showPhoto(I)V

    .line 51
    new-instance v5, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-direct {v5, v7}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 52
    .local v5, "ll":Landroid/widget/LinearLayout;
    const v7, 0x7f0b006a

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/HorizontalScrollView;

    .line 54
    .local v1, "hsv":Landroid/widget/HorizontalScrollView;
    sget-boolean v7, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->inited:Z

    if-nez v7, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v7}, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->initMinmap(Landroid/content/res/Resources;)V

    .line 56
    const/4 v7, 0x1

    sput-boolean v7, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->inited:Z

    .line 58
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v7, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->minmapCache:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 59
    move v3, v2

    .line 60
    .local v3, "index":I
    sget-object v7, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->minmapCache:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 61
    .local v0, "bmp":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-direct {v4, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 62
    .local v4, "iv":Landroid/widget/ImageView;
    const/4 v7, 0x3

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x2

    invoke-virtual {v4, v7, v8, v9, v10}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 63
    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 64
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 65
    new-instance v7, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment$1;

    invoke-direct {v7, p0, v3}, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment$1;-><init>(Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;I)V

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    const/4 v7, -0x1

    const/4 v8, -0x1

    invoke-virtual {v5, v4, v7, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    .line 58
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 73
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    .end local v3    # "index":I
    .end local v4    # "iv":Landroid/widget/ImageView;
    :cond_1
    const/4 v7, -0x1

    const/4 v8, -0x1

    invoke-virtual {v1, v5, v7, v8}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;II)V

    .line 74
    return-object v6
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 116
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 117
    return-void
.end method
