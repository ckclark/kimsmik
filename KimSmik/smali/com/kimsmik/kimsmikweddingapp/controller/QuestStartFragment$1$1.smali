.class Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1$1;
.super Ljava/lang/Object;
.source "QuestStartFragment.java"

# interfaces
.implements Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1;

.field final synthetic val$quesFrag:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;


# direct methods
.method constructor <init>(Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1;Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1$1;->this$1:Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1;

    iput-object p2, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1$1;->val$quesFrag:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnFinish(Z)V
    .locals 5
    .param p1, "clear"    # Z

    .prologue
    const/4 v4, 0x0

    .line 53
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1$1;->this$1:Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1;

    iget-object v1, v1, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;

    # getter for: Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->uiContainer:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->access$000(Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 54
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1$1;->this$1:Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1;

    iget-object v1, v1, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;

    invoke-virtual {v1}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1$1;->val$quesFrag:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->detach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 55
    if-eqz p1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1$1;->this$1:Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1;

    iget-object v1, v1, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;

    invoke-virtual {v1}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/FragmentActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 57
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    # getter for: Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->PREF_STAGE_CLEAR:Ljava/lang/String;
    invoke-static {}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->access$100()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 58
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1$1;->this$1:Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1;

    iget-object v1, v1, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment$1;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;

    # getter for: Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->clearView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;->access$200(Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 60
    .end local v0    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method
