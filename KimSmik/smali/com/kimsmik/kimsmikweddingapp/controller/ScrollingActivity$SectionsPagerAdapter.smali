.class public Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$SectionsPagerAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "ScrollingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SectionsPagerAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;


# direct methods
.method public constructor <init>(Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;Landroid/support/v4/app/FragmentManager;)V
    .locals 0
    .param p2, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$SectionsPagerAdapter;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;

    .line 116
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 117
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x3

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 123
    add-int/lit8 v0, p1, 0x1

    invoke-static {v0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$PlaceholderFragment;->newInstance(I)Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$PlaceholderFragment;

    move-result-object v0

    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 134
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 135
    .local v0, "l":Ljava/util/Locale;
    packed-switch p1, :pswitch_data_0

    .line 143
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 137
    :pswitch_0
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$SectionsPagerAdapter;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;

    const v2, 0x7f0d004f

    invoke-virtual {v1, v2}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 139
    :pswitch_1
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$SectionsPagerAdapter;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;

    const v2, 0x7f0d0050

    invoke-virtual {v1, v2}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 141
    :pswitch_2
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$SectionsPagerAdapter;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;

    const v2, 0x7f0d0051

    invoke-virtual {v1, v2}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
