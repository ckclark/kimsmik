.class public Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFrameFragment;
.super Landroid/support/v4/app/Fragment;
.source "QuestionFrameFragment.java"


# instance fields
.field private act:Landroid/support/v4/app/FragmentActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 26
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFrameFragment;->act:Landroid/support/v4/app/FragmentActivity;

    .line 27
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 19
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 20
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    const v1, 0x7f040024

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 33
    .local v0, "root":Landroid/view/View;
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFrameFragment;->act:Landroid/support/v4/app/FragmentActivity;

    if-eqz v1, :cond_0

    .line 34
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFrameFragment;->act:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0b007c

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;

    .line 46
    :cond_0
    return-object v0
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 51
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 53
    return-void
.end method
