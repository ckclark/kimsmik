.class public Lcom/kimsmik/kimsmikweddingapp/controller/MenuFragment;
.super Landroid/support/v4/app/Fragment;
.source "MenuFragment.java"

# interfaces
.implements Lcom/kimsmik/kimsmikweddingapp/IMenuFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 17
    return-void
.end method


# virtual methods
.method public GetTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    const-string v0, "\u5a5a\u5bb4\u83dc\u55ae"

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 22
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    const v0, 0x7f040021

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 37
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 38
    return-void
.end method
