.class public Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$PlaceholderFragment;
.super Landroid/support/v4/app/Fragment;
.source "ScrollingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PlaceholderFragment"
.end annotation


# static fields
.field private static final ARG_SECTION_NUMBER:Ljava/lang/String; = "section_number"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 170
    return-void
.end method

.method public static newInstance(I)Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$PlaceholderFragment;
    .locals 3
    .param p0, "sectionNumber"    # I

    .prologue
    .line 162
    new-instance v1, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$PlaceholderFragment;

    invoke-direct {v1}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$PlaceholderFragment;-><init>()V

    .line 163
    .local v1, "fragment":Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$PlaceholderFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 164
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "section_number"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 165
    invoke-virtual {v1, v0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$PlaceholderFragment;->setArguments(Landroid/os/Bundle;)V

    .line 166
    return-object v1
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, -0x2

    .line 175
    const v4, 0x7f04001e

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 176
    .local v1, "rootView":Landroid/view/View;
    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 177
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$PlaceholderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "section_number"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 178
    .local v3, "sectionNumber":I
    const v4, 0x7f0b006d

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 179
    .local v2, "sectionLabel":Landroid/widget/TextView;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Section : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 182
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$PlaceholderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 183
    .local v0, "btn":Landroid/widget/Button;
    const-string v4, "Scrolling Text"

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 184
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 187
    new-instance v4, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$PlaceholderFragment$1;

    invoke-direct {v4, p0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$PlaceholderFragment$1;-><init>(Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$PlaceholderFragment;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v4, v1

    .line 195
    check-cast v4, Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 198
    .end local v0    # "btn":Landroid/widget/Button;
    :cond_0
    return-object v1
.end method
