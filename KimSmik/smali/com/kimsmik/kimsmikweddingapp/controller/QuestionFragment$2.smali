.class Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$2;
.super Ljava/lang/Object;
.source "QuestionFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->SetNextQuestion()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;


# direct methods
.method constructor <init>(Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$2;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 135
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$2;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;

    # getter for: Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->finishListener:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;
    invoke-static {v0}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->access$100(Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;)Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$2;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;

    # getter for: Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->correctNum:I
    invoke-static {v0}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->access$200(Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;)I

    move-result v0

    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$2;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;

    # getter for: Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->questions:Ljava/util/List;
    invoke-static {v1}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->access$300(Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 137
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$2;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;

    # getter for: Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->finishListener:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;
    invoke-static {v0}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->access$100(Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;)Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;->OnFinish(Z)V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$2;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;

    # getter for: Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->finishListener:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;
    invoke-static {v0}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->access$100(Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;)Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;->OnFinish(Z)V

    goto :goto_0
.end method
