.class public Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "MainActivity.java"

# interfaces
.implements Landroid/support/v7/app/ActionBar$TabListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;
    }
.end annotation


# static fields
.field private static final PLAY_SERVICES_RESOLUTION_REQUEST:I = 0x2328


# instance fields
.field mSectionsPagerAdapter:Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;

.field mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 150
    return-void
.end method

.method private checkPlayServices()Z
    .locals 2

    .prologue
    .line 98
    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    .line 99
    .local v0, "resultCode":I
    if-eqz v0, :cond_1

    .line 100
    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isUserRecoverableError(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    const/16 v1, 0x2328

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getErrorDialog(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 105
    :goto_0
    const/4 v1, 0x0

    .line 107
    :goto_1
    return v1

    .line 103
    :cond_0
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->finish()V

    goto :goto_0

    .line 107
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 47
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v3, 0x7f040019

    invoke-virtual {p0, v3}, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->setContentView(I)V

    .line 51
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 52
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setNavigationMode(I)V

    .line 53
    invoke-virtual {v0, v4}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 54
    invoke-virtual {v0, v4}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 61
    new-instance v3, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;

    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;-><init>(Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;Landroid/support/v4/app/FragmentManager;)V

    iput-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->mSectionsPagerAdapter:Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;

    .line 64
    const v3, 0x7f0b0067

    invoke-virtual {p0, v3}, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    iput-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 65
    iget-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->mSectionsPagerAdapter:Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 71
    iget-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v4, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$1;

    invoke-direct {v4, p0, v0}, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$1;-><init>(Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;Landroid/support/v7/app/ActionBar;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 79
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->mSectionsPagerAdapter:Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;

    invoke-virtual {v3}, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;->getCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 84
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->newTab()Landroid/support/v7/app/ActionBar$Tab;

    move-result-object v3

    iget-object v4, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->mSectionsPagerAdapter:Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;

    invoke-virtual {v4, v1}, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v7/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroid/support/v7/app/ActionBar$Tab;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/support/v7/app/ActionBar$Tab;->setTabListener(Landroid/support/v7/app/ActionBar$TabListener;)Landroid/support/v7/app/ActionBar$Tab;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->addTab(Landroid/support/v7/app/ActionBar$Tab;)V

    .line 79
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 90
    :cond_0
    invoke-direct {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->checkPlayServices()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 92
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/kimsmik/kimsmikweddingapp/service/GCMIntentService;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 93
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 95
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 113
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 121
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 124
    .local v0, "id":I
    const v1, 0x7f0b0097

    if-ne v0, v1, :cond_0

    .line 125
    const/4 v1, 0x1

    .line 128
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method public onTabReselected(Landroid/support/v7/app/ActionBar$Tab;Landroid/support/v4/app/FragmentTransaction;)V
    .locals 0
    .param p1, "tab"    # Landroid/support/v7/app/ActionBar$Tab;
    .param p2, "fragmentTransaction"    # Landroid/support/v4/app/FragmentTransaction;

    .prologue
    .line 144
    return-void
.end method

.method public onTabSelected(Landroid/support/v7/app/ActionBar$Tab;Landroid/support/v4/app/FragmentTransaction;)V
    .locals 2
    .param p1, "tab"    # Landroid/support/v7/app/ActionBar$Tab;
    .param p2, "fragmentTransaction"    # Landroid/support/v4/app/FragmentTransaction;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p1}, Landroid/support/v7/app/ActionBar$Tab;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 136
    return-void
.end method

.method public onTabUnselected(Landroid/support/v7/app/ActionBar$Tab;Landroid/support/v4/app/FragmentTransaction;)V
    .locals 0
    .param p1, "tab"    # Landroid/support/v7/app/ActionBar$Tab;
    .param p2, "fragmentTransaction"    # Landroid/support/v4/app/FragmentTransaction;

    .prologue
    .line 140
    return-void
.end method
