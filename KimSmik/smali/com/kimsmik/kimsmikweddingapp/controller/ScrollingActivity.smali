.class public Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "ScrollingActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$PlaceholderFragment;,
        Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$SectionsPagerAdapter;
    }
.end annotation


# static fields
.field private static final PLAY_SERVICES_RESOLUTION_REQUEST:I = 0x2328


# instance fields
.field mSectionsPagerAdapter:Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$SectionsPagerAdapter;

.field mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 150
    return-void
.end method

.method private checkPlayServices()Z
    .locals 2

    .prologue
    .line 97
    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    .line 98
    .local v0, "resultCode":I
    if-eqz v0, :cond_1

    .line 99
    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isUserRecoverableError(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    const/16 v1, 0x2328

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getErrorDialog(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 104
    :goto_0
    const/4 v1, 0x0

    .line 106
    :goto_1
    return v1

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;->finish()V

    goto :goto_0

    .line 106
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const v2, 0x7f04001a

    invoke-virtual {p0, v2}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;->setContentView(I)V

    .line 49
    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 50
    .local v0, "ani":Landroid/animation/ValueAnimator;
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 51
    new-instance v2, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$1;

    invoke-direct {v2, p0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$1;-><init>(Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 60
    new-instance v2, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$SectionsPagerAdapter;

    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$SectionsPagerAdapter;-><init>(Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;Landroid/support/v4/app/FragmentManager;)V

    iput-object v2, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;->mSectionsPagerAdapter:Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$SectionsPagerAdapter;

    .line 62
    const v2, 0x7f0b0067

    invoke-virtual {p0, v2}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ViewPager;

    iput-object v2, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 63
    iget-object v2, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;->mSectionsPagerAdapter:Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity$SectionsPagerAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 66
    invoke-direct {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;->checkPlayServices()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 68
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/kimsmik/kimsmikweddingapp/service/GCMIntentService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 69
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 71
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    return-void

    .line 49
    :array_0
    .array-data 4
        0x0
        0x44fa0000    # 2000.0f
    .end array-data
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 86
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 89
    .local v0, "id":I
    const v1, 0x7f0b0097

    if-ne v0, v1, :cond_0

    .line 90
    const/4 v1, 0x1

    .line 93
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method
