.class public Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;
.super Landroid/app/Activity;
.source "ScrollingTextActivity.java"


# static fields
.field private static final HIDER_FLAGS:I = 0x6


# instance fields
.field private camera:Landroid/hardware/Camera;

.field private isLightOn:Z

.field mHideHandler:Landroid/os/Handler;

.field mHideRunnable:Ljava/lang/Runnable;

.field private mSystemUiHider:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHider;

.field private srollingText:Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->isLightOn:Z

    .line 119
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->mHideHandler:Landroid/os/Handler;

    .line 120
    new-instance v0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity$1;

    invoke-direct {v0, p0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity$1;-><init>(Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;)V

    iput-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->mHideRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;)Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHider;
    .locals 1
    .param p0, "x0"    # Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->mSystemUiHider:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHider;

    return-object v0
.end method

.method private delayedHide(I)V
    .locals 4
    .param p1, "delayMillis"    # I

    .prologue
    .line 131
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->mHideHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->mHideRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 132
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->mHideHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->mHideRunnable:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 133
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->hide()V

    .line 47
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "message"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "message":Ljava/lang/String;
    const v2, 0x7f04001b

    invoke-virtual {p0, v2}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->setContentView(I)V

    .line 49
    const v2, 0x7f0b0068

    invoke-virtual {p0, v2}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;

    iput-object v2, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->srollingText:Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;

    .line 50
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 51
    iget-object v2, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->srollingText:Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;

    invoke-virtual {v2, v1}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    :cond_0
    iget-object v2, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->srollingText:Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Lcom/kimsmik/kimsmikweddingapp/object/MarqueeTextView;->scrollText(F)V

    .line 54
    const v2, 0x7f0b0066

    invoke-virtual {p0, v2}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 57
    .local v0, "bgView":Landroid/view/View;
    const/4 v2, 0x6

    invoke-static {p0, v0, v2}, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHider;->getInstance(Landroid/app/Activity;Landroid/view/View;I)Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHider;

    move-result-object v2

    iput-object v2, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->mSystemUiHider:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHider;

    .line 58
    iget-object v2, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->mSystemUiHider:Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHider;

    invoke-virtual {v2}, Lcom/kimsmik/kimsmikweddingapp/util/SystemUiHider;->setup()V

    .line 59
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 83
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 85
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->camera:Landroid/hardware/Camera;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->isLightOn:Z

    if-eqz v1, :cond_0

    .line 86
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 87
    .local v0, "p":Landroid/hardware/Camera$Parameters;
    const-string v1, "off"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 88
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 89
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->stopPreview()V

    .line 90
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->isLightOn:Z

    .line 92
    .end local v0    # "p":Landroid/hardware/Camera$Parameters;
    :cond_0
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->camera:Landroid/hardware/Camera;

    if-eqz v1, :cond_1

    .line 93
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    .line 94
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->camera:Landroid/hardware/Camera;

    .line 96
    :cond_1
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/app/Activity;->onPostCreate(Landroid/os/Bundle;)V

    .line 116
    const/16 v0, 0x64

    invoke-direct {p0, v0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->delayedHide(I)V

    .line 117
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 68
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 69
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "android.hardware.camera"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v1

    iput-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->camera:Landroid/hardware/Camera;

    .line 72
    :cond_0
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->camera:Landroid/hardware/Camera;

    if-eqz v1, :cond_1

    .line 73
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 74
    .local v0, "p":Landroid/hardware/Camera$Parameters;
    const-string v1, "torch"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 75
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 76
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->startPreview()V

    .line 77
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/ScrollingTextActivity;->isLightOn:Z

    .line 79
    .end local v0    # "p":Landroid/hardware/Camera$Parameters;
    :cond_1
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 64
    return-void
.end method
