.class public Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;
.super Landroid/support/v4/app/Fragment;
.source "QuestionFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;,
        Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    }
.end annotation


# instance fields
.field private correctNum:I

.field private finishListener:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;

.field private nowQuestIndex:I

.field private optionGroup:Landroid/widget/LinearLayout;

.field private progressText:Landroid/widget/TextView;

.field private questText:Landroid/widget/TextView;

.field private questions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->questions:Ljava/util/List;

    .line 36
    iput v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->nowQuestIndex:I

    .line 37
    iput v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->correctNum:I

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->finishListener:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;

    .line 42
    return-void
.end method

.method private CheckAns(Landroid/widget/TextView;)V
    .locals 3
    .param p1, "tv"    # Landroid/widget/TextView;

    .prologue
    .line 122
    iget-object v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->questions:Ljava/util/List;

    iget v2, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->nowQuestIndex:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;

    .line 123
    .local v0, "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, v0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;->answer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    iget v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->correctNum:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->correctNum:I

    .line 125
    :cond_0
    iget v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->nowQuestIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->nowQuestIndex:I

    .line 126
    invoke-direct {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->SetNextQuestion()V

    .line 128
    return-void
.end method

.method private GetResultString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 168
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u7b54\u5c0d\u984c\u6578 : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->correctNum:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->questions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "msg":Ljava/lang/String;
    iget v2, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->correctNum:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->questions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 170
    .local v1, "persent":F
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_0

    .line 171
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u606d\u559c\u4f60\u7b54\u5c0d\u6240\u6709\u984c\u76ee\n\u4f60\u771f\u662f\u592a\u4e86\u89e3\u65b0\u4eba\u4e86\n\u5a5a\u79ae\u7576\u5929\u8acb\u81f3\u73fe\u5834\n\u51fa\u793aApp\u5167\u7684\u901a\u95dc\u8b49\u660e\n\u63db\u53d6\u795e\u7955\u5c0f\u79ae"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 180
    :goto_0
    return-object v0

    .line 172
    :cond_0
    float-to-double v2, v1

    const-wide v4, 0x3fe999999999999aL    # 0.8

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_1

    .line 173
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u771f\u662f\u592a\u53ef\u60dc\u4e86\n\u53ea\u5dee\u4e00\u9ede\u9ede\u5c31\u5168\u90e8\u6b63\u89e3\u4e86\n\u8acb\u518d\u63a5\u518d\u53b2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 174
    :cond_1
    float-to-double v2, v1

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_2

    .line 175
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u770b\u4f86\u4f60\u5c0d\u65b0\u4eba\u9084\u662f\u4e00\u77e5\u534a\u89e3\u7684\u5594\n\u4f46\u6c92\u95dc\u4fc2\n\u8acb\u518d\u591a\u8a66\u5e7e\u6b21\n\u4e5f\u53ef\u66f4\u4e86\u89e3\u65b0\u4eba\u5594"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 177
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\u54ce\u5440\n\u4f60\u5c0d\u65b0\u4eba\u4e0d\u662f\u5f88\u4e86\u89e3\u7684\u6a23\u5b50\u5462\n\u9019\u6a23\u5b50\u4e0d\u592a\u884c\u5537\n\u8acb\u591a\u8a66\u5e7e\u6b21\n\u4e5f\u8a31\u4f60\u6703\u6162\u6162\u4e86\u89e3\u65b0\u4eba\u5594"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private SetNextQuestion()V
    .locals 7

    .prologue
    .line 130
    iget v4, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->nowQuestIndex:I

    iget-object v5, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->questions:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lt v4, v5, :cond_0

    .line 131
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 132
    .local v0, "dialogBuilder":Landroid/support/v7/app/AlertDialog$Builder;
    const-string v4, "\u78ba\u5b9a"

    new-instance v5, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$2;

    invoke-direct {v5, p0}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$2;-><init>(Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;)V

    invoke-virtual {v0, v4, v5}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 143
    invoke-direct {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->GetResultString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 144
    const-string v4, "\u7b54\u984c\u7d50\u679c"

    invoke-virtual {v0, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 145
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 165
    .end local v0    # "dialogBuilder":Landroid/support/v7/app/AlertDialog$Builder;
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v4, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->questions:Ljava/util/List;

    iget v5, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->nowQuestIndex:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;

    .line 148
    .local v3, "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    iget-object v4, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->questText:Landroid/widget/TextView;

    iget-object v5, v3, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;->quest:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->optionGroup:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 150
    iget-object v4, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->optionGroup:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 151
    .local v2, "opt":Landroid/widget/TextView;
    iget-object v4, v3, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;->option:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 152
    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 149
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 154
    :cond_1
    iget-object v4, v3, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;->option:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 163
    .end local v2    # "opt":Landroid/widget/TextView;
    :cond_2
    iget-object v4, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->progressText:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->nowQuestIndex:I

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->questions:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;Landroid/widget/TextView;)V
    .locals 0
    .param p0, "x0"    # Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->CheckAns(Landroid/widget/TextView;)V

    return-void
.end method

.method static synthetic access$100(Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;)Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;
    .locals 1
    .param p0, "x0"    # Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->finishListener:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;

    .prologue
    .line 25
    iget v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->correctNum:I

    return v0
.end method

.method static synthetic access$300(Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->questions:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public SetOnQuizFinishListener(Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;)V
    .locals 0
    .param p1, "l"    # Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->finishListener:Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$QuizFinishListener;

    .line 46
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 51
    invoke-virtual {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f060000

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v2

    .line 53
    .local v2, "parser":Landroid/content/res/XmlResourceParser;
    const/4 v3, 0x0

    .local v3, "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    move-object v4, v3

    .line 54
    .end local v3    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    .local v4, "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_4

    .line 56
    invoke-interface {v2}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_2

    .line 57
    invoke-interface {v2}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Quest"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 58
    new-instance v3, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;

    invoke-direct {v3, p0}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;-><init>(Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 59
    .end local v4    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    .restart local v3    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    const/4 v6, 0x0

    :try_start_1
    const-string v7, "text"

    invoke-interface {v2, v6, v7}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;->quest:Ljava/lang/String;

    .line 73
    :goto_1
    invoke-interface {v2}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v4, v3

    .end local v3    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    .restart local v4    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    goto :goto_0

    .line 60
    :cond_0
    :try_start_2
    invoke-interface {v2}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Option"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 61
    const/4 v6, 0x0

    const-string v7, "text"

    invoke-interface {v2, v6, v7}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 62
    .local v5, "text":Ljava/lang/String;
    iget-object v6, v4, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;->option:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    const/4 v6, 0x0

    const-string v7, "answer"

    const/4 v8, 0x0

    invoke-interface {v2, v6, v7, v8}, Landroid/content/res/XmlResourceParser;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    .line 64
    .local v1, "isAnswer":Z
    if-eqz v1, :cond_1

    .line 65
    iput-object v5, v4, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;->answer:Ljava/lang/String;

    :cond_1
    move-object v3, v4

    .line 66
    .end local v4    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    .restart local v3    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    goto :goto_1

    .line 68
    .end local v1    # "isAnswer":Z
    .end local v3    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    .end local v5    # "text":Ljava/lang/String;
    .restart local v4    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    :cond_2
    invoke-interface {v2}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_3

    .line 69
    invoke-interface {v2}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Quest"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 70
    iget-object v6, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->questions:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_3
    move-object v3, v4

    .end local v4    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    .restart local v3    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    goto :goto_1

    .line 75
    .end local v3    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    .restart local v4    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    :catch_0
    move-exception v0

    move-object v3, v4

    .line 76
    .end local v4    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v3    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    :goto_2
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 80
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .end local v3    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    :cond_4
    :goto_3
    return-void

    .line 77
    .restart local v4    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    :catch_1
    move-exception v0

    move-object v3, v4

    .line 78
    .end local v4    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    .local v0, "e":Ljava/io/IOException;
    .restart local v3    # "quest":Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$Question;
    :goto_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 77
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    goto :goto_4

    .line 75
    :catch_3
    move-exception v0

    goto :goto_2
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 86
    iput v4, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->nowQuestIndex:I

    .line 87
    iput v4, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->correctNum:I

    .line 88
    const v3, 0x7f040023

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 90
    .local v2, "view":Landroid/view/View;
    const v3, 0x7f0b0078

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->optionGroup:Landroid/widget/LinearLayout;

    .line 91
    const v3, 0x7f0b0077

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->questText:Landroid/widget/TextView;

    .line 92
    const v3, 0x7f0b0075

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->progressText:Landroid/widget/TextView;

    .line 94
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->optionGroup:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 95
    iget-object v3, p0, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->optionGroup:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 96
    .local v1, "opt":Landroid/widget/TextView;
    new-instance v3, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$1;

    invoke-direct {v3, p0}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment$1;-><init>(Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    .end local v1    # "opt":Landroid/widget/TextView;
    :cond_0
    invoke-direct {p0}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestionFragment;->SetNextQuestion()V

    .line 115
    return-object v2
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 189
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 190
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 184
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 185
    return-void
.end method
