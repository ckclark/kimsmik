.class public Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SectionsPagerAdapter"
.end annotation


# instance fields
.field menuFragments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/kimsmik/kimsmikweddingapp/IMenuFragment;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;


# direct methods
.method public constructor <init>(Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;Landroid/support/v4/app/FragmentManager;)V
    .locals 2
    .param p2, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;->this$0:Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity;

    .line 153
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;->menuFragments:Ljava/util/List;

    .line 154
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;->menuFragments:Ljava/util/List;

    new-instance v1, Lcom/kimsmik/kimsmikweddingapp/controller/HomeFragment;

    invoke-direct {v1}, Lcom/kimsmik/kimsmikweddingapp/controller/HomeFragment;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;->menuFragments:Ljava/util/List;

    new-instance v1, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;

    invoke-direct {v1}, Lcom/kimsmik/kimsmikweddingapp/controller/QuestStartFragment;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;->menuFragments:Ljava/util/List;

    new-instance v1, Lcom/kimsmik/kimsmikweddingapp/controller/MapFragment;

    invoke-direct {v1}, Lcom/kimsmik/kimsmikweddingapp/controller/MapFragment;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;->menuFragments:Ljava/util/List;

    new-instance v1, Lcom/kimsmik/kimsmikweddingapp/controller/MenuFragment;

    invoke-direct {v1}, Lcom/kimsmik/kimsmikweddingapp/controller/MenuFragment;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;->menuFragments:Ljava/util/List;

    new-instance v1, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;

    invoke-direct {v1}, Lcom/kimsmik/kimsmikweddingapp/controller/GalleryFragment;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;->menuFragments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 165
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;->menuFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    check-cast v0, Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 175
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;->menuFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/kimsmik/kimsmikweddingapp/controller/MainActivity$SectionsPagerAdapter;->menuFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/kimsmik/kimsmikweddingapp/IMenuFragment;

    invoke-interface {v0}, Lcom/kimsmik/kimsmikweddingapp/IMenuFragment;->GetTitle()Ljava/lang/String;

    move-result-object v0

    .line 177
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
